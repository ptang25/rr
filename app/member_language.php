<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member_language extends Model
{
    protected $table = "member_languages";

    protected $fillable = [
        'user_id', 'language'
    ];
}
