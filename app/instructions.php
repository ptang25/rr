<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instructions extends Model
{
    protected $table = "instructions";

    protected $fillable = [
        'header', 'pre_image', 'pre_text', 'post_image', 'post_text', 'footer'
    ];
}
