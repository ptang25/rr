<?php

namespace App\Http\Controllers;

use App\matchup;
use App\UserAlliance;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use DateTime;

class MatchupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userID = Auth::user()->id;

        $alliance = UserAlliance::where('uid', $userID)->first();

        $matchup_date = matchup::where([
            ['date', '>', Carbon::now()->toDateTimeString()],
            ['aid', '=', $alliance->id]
        ])->get();
        
        //No Matchup in the next 3 days 
        if (count($matchup_date) <= 0) {
            return view('matchups', ['allianceID' => $alliance->id]);   
        }

        else {
            //error_log("Match Future : " . $matchup_date->date);

            $dt = new DateTime($matchup_date[0]->date);

            $date = $dt->format('Y-m-d');
            $timehour = $dt->format('H');
            $timemin = $dt->format('i');

            error_log("Date : " . $date);
            error_log("Hour : " . $timehour);
            error_log("Minute : " . $timemin);

            $team = $matchup_date[0]->team;
            
            return view('matchups', ['allianceID' => $alliance->id, 'matchupID' => $matchup_date[0]->id, 'date' => $date, 'hour' => $timehour, 'minute' => $timemin, 'team' => $team]);
        }
    }

    public function storeMatchup (Request $request) {

        if ($request->input('matchup_id') != null) {
            $match = matchup::find($request->input('matchup_id'));
            error_log("ID is : " . $match->id);
        }
        //Get Next ID
        else {
            $match = new matchup();
        }

        //Get alliance ID
        $allianceID = $request->input('alliance_id');

        //error_log("My Config ID is : " . $option->id);
        $date = $request->input('date');
        $timeHH = $request->input('hour');
        $timeMM = $request->input('minute');
        $teamcolor = $request->input('teamcolor');

        $matchup_date = date_create($date . " " . $timeHH . ":" . $timeMM . ":00");

        error_log("Alliance is : " . $allianceID);
        error_log("Date is : " . date_format($matchup_date,"Y-m-d H:i:s"));
        error_log("TimeHH is : " . $timeHH);
        error_log("TimeMM is : " . $timeMM);
        error_log("Team is : " . $teamcolor);

        $match->aid = $allianceID;
        $match->date = date_format($matchup_date,"Y-m-d H:i:s");
        $match->team = $teamcolor;

        $match->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\matchup  $matchup
     * @return \Illuminate\Http\Response
     */
    public function show(matchup $matchup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\matchup  $matchup
     * @return \Illuminate\Http\Response
     */
    public function edit(matchup $matchup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\matchup  $matchup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, matchup $matchup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\matchup  $matchup
     * @return \Illuminate\Http\Response
     */
    public function destroy(matchup $matchup)
    {
        //
    }
}
