<?php

namespace App\Http\Controllers;

use App\instructions;
use App\instructions_es;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stichoza\GoogleTranslate\GoogleTranslate;


class StrategyController extends Controller
{
    public function viewStrategy($alliance_id) {

        $strategies = instructions::where('aid', $alliance_id)->get();
    
        return view('strategy', ['alliance_id' => $alliance_id , 'strategies' => $strategies]);
    
    }
    
    //Edit Strategy
    public function editStrategy($alliance_id, $strategy_id) {

        $strategies = instructions::where('aid', $alliance_id)->get();
    
        $strategy = instructions::find($strategy_id);

        return view('strategy', ['alliance_id' => $alliance_id , 'strategies' => $strategies, 'instructions' => $strategy]);
    
    }

    public function createStrategy(Request $request) {
    
        $strategy = instructions::find($request->input('instructions_id'));

        if ($strategy == null) {
            $strategy = new instructions();
        }

        $strategy->type = $request->input('group');
        $strategy->aid = $request->input('alliance_id');
        $strategy->header = $request->input('top_instructions');
        $strategy->pre_text = $request->input('stage1_instructions');
        $strategy->post_text = $request->input('stage2_instructions');
        $strategy->footer = $request->input('footer');

        //Team
        $strategy->color = $request->input('team');

        error_log("Team Color: " . $strategy->color);

        //Get Current ID that is being saved
        $statement = DB::select("SHOW TABLE STATUS LIKE 'instructions'");
        $nextId = $statement[0]->Auto_increment;

        //Upload Image
        if ($request->has('pre_image')) {

            $image = $request->file('pre_image');
            
            error_log("Image: " . $image);

            $request->pre_image->storeAs($nextId, $request->pre_image->getClientOriginalName());

            $my_image_dir = "storage/" . $nextId . "/";
            $strategy->pre_image = $my_image_dir . $image->getClientOriginalName();
        }

        if ($request->has('post_image')) {

            $image = $request->file('post_image');
            
            $request->post_image->storeAs($nextId, $request->post_image->getClientOriginalName());

            $my_image_dir = "storage/" . $nextId . "/";
            $strategy->post_image = $my_image_dir . $image->getClientOriginalName();
        }

        $strategy->save();
    
        //Copy Over To Spanish
        $strategy_es = instructions_es::find($strategy->id);

        if ($strategy_es == null) {
            $strategy_es = new instructions_es();
        }

        $tr = new GoogleTranslate();
        $tr->setTarget('es'); 

        $strategy_es->id = $strategy->id;
        $strategy_es->type = $strategy->type;
        $strategy_es->aid = $strategy->aid;
        $strategy_es->color = $strategy->color;
        $strategy_es->header = $tr->translate($strategy->header);
        $strategy_es->pre_text = $tr->translate($strategy->pre_text);
        $strategy_es->post_text = $tr->translate($strategy->post_text);
        $strategy_es->footer = $tr->translate($strategy->footer);
        $strategy_es->pre_image = $strategy->pre_image;
        $strategy_es->post_image = $strategy->post_image;

        $strategy_es->save();

        $strategies = instructions::where('aid', $strategy->aid)->get();
    
        return view('strategy', ['alliance_id' => $strategy->aid , 'strategies' => $strategies]);
    }

}
