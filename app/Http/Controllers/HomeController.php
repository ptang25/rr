<?php

namespace App\Http\Controllers;

use App\UserAlliance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userID = Auth::user()->id;

        $alliance = UserAlliance::where('uid', $userID)->first();
        
        return view('home', ['alliance' => $alliance]);
    }
}
