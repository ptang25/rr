<?php

namespace App\Http\Controllers;

use App\instructions;
use App\instructions_es;
use App\matchup;
use App\member;
use App\member_language;
use App\reservation;
use App\timezones;
use App\UserAlliance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Redirect;

class ReservationController extends Controller
{
    //Global Variable
    private $off_set_checkin = 24;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($alliance_id)
    {
        $members = member::where('aid', $alliance_id)->orderBy('name', 'asc')->get();

        $matchup_date = matchup::where([
            ['date', '>', Carbon::now()->toDateTimeString()],
            ['aid', '=', $alliance_id]
        ])->get();        

        if (sizeof($matchup_date)> 0) {

            $reservation = reservation::where([
                ['matchup_id', '=', $matchup_date[0]->id],
                ['reservation_status', '=', 1]
            ])->get();

            return view('reservation', ['alliance_id' => $alliance_id , 'members' => $members, 'matchup' => $matchup_date[0], 'reservations' => sizeof($reservation)]);
        }
        else {
            return view('reservation', ['alliance_id' => $alliance_id , 'members' => $members]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Return Status of Member
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function memberStatus($name, $alliance_id) 
    {
        error_log("Alliance : " . $alliance_id);
        error_log("Name : " . $name);

        $matchup = matchup::where([
            ['date', '>', Carbon::now()->toDateTimeString()],
            ['aid', '=', $alliance_id]
        ])->first();

        error_log("Matchup : " . $matchup);
        
        //Get Member ID
        $member = member::where([
            ['aid', '=', $alliance_id],
            ['name', '=', $name]
        ])->get();

        //If we are able to find the member
        if (sizeof($member) > 0) {

            error_log("Member : " . $member);

            $reservation = reservation::where([
                ['matchup_id', '=', $matchup->id],
                ['member_id', '=', $member[0]->id]
            ])->get();

            error_log("After :" . sizeof($reservation));
        }

        //If there is a reservation
        if (sizeof($reservation) > 0) {

            //First Record
            $reservation = $reservation[0];

            error_log("Reservation : " . $reservation);

            error_log("Reservation Status : " . $reservation->reservation_status);
            error_log("Checkin Status : " . $reservation->checkin_status);

            error_log("Now : " . Carbon::now()->toDateTimeString());
            error_log("Time Offset : " . Carbon::now()->addHour($this->off_set_checkin)->toDateTimeString());
            error_log("Matchup Time : " . $matchup->date);

            //Checkin Time
            if ($matchup->date <= Carbon::now()->addHour($this->off_set_checkin)->toDateTimeString()) {
                
                //Already Assigned Player
                if ($reservation->player_assignment) {
                    $statement = "View my personalized strategy?";
                    $font_color = "green";
                }

                //Already Assigned Rally Leader
                else if ($reservation->rally_assignment) {
                    $statement = "View my personalized strategy?";
                    $font_color = "green";
                }

                else if ($reservation->checkin_status == 1) {

                    $statement = "Ready to Check-in?";
                    $font_color = "green";
                }
            
                else if ($reservation->reservation_status == 1) {
                    $statement = "Our apologies, you were not selected to the Raid this time, we will fit in the next one, thank you for reserving.";
                    $font_color = "red";
                }

                else {
                    $statement = "Cutoff Reservation Time has expired, Sorry!";
                    $font_color = "red";
                }
            }
                

            else {
                //Change status to unavailable
                if ($reservation->reservation_status == 1) {
                    $statement = "You have already indicated you will be available, would you like to change your status to Unavailable?";
                    $font_color = "red";
                }
                //Change status back to available
                else if ($reservation->reservation_status == 0) {
                    $statement = "Are you available and interested in attending this Reservoir Raid at the above Date and Time?";
                    $font_color = "blue";
                }
            }
        }
        else {

            if ($matchup->date <= Carbon::now()->addHour($this->off_set_checkin)->toDateTimeString()) {
                $statement = "Cutoff Reservation Time has expired, Sorry!";
                $font_color = "red";
            }

            else {
                $statement = "Are you available at the above time and interested in attending this Reservoir Raid?";
                $font_color = "blue";
            }
        }

        error_log("Statement : " . $statement);

        $data = array();
        $data['statement'] = $statement;
        $data['color']    = $font_color;

        return response()->json($data);   
    }

    /**
     * Return TimeZone of DAte provided
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTimeZone($tid, $id) 
    {

        $date = matchup::find($id);
        $timezone_value = timezones::find($tid);

        error_log("Timezone : " . $timezone_value->value);
        error_log("Date : " . $date->date);

        $userTimezone = new DateTimeZone($timezone_value->value);       
        $userDateTime = new DateTime($date->date);
        $userDateTime->setTimezone($userTimezone);
       
        $userdate = $userDateTime->format('Y-M-d H:i:s'); // output = 2017-01-01 00:00:00

        error_log("New Date: " . $userdate);

        return response()->json($userdate);   
    }

    /**
     * Return TimeZone of DAte provided
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setReservation(Request $request) 
    {

        $alliance_id = $request->input('alliance_id');
        $member_name = $request->input('member_name');
        
        $matchup = matchup::where([
            ['date', '>', Carbon::now()->toDateTimeString()],
            ['aid', '=', $alliance_id]
        ])->get();

        //No Available Matchup
        if (sizeof($matchup) < 1) {

        } 

        else {
            //Get Member ID
            $member = member::where([
                ['aid', '=', $alliance_id],
                ['name', '=', $member_name]
            ])->get();

            //If we are able to find the member
            if (sizeof($member) > 0) {

                $reservation = reservation::where([
                    ['matchup_id', '=', $matchup[0]->id],
                    ['member_id', '=', $member[0]->id]
                ])->get();
            }

            error_log($reservation);

            //If there is a reservation
            if (sizeof($reservation) > 0) {

                $reservation = $reservation[0];

                //Check to see if your confirmed first
                if ($matchup[0]->date <= Carbon::now()->addHour($this->off_set_checkin)->toDateTimeString()) {

                    if ($reservation->checkin_status == 1) {

                        //If Rally Leader
                        if ($reservation->rally_assignment != null) {

                            //Send player to rally view
                        }

                        //Not a Rally Leader and no number has been assigned
                        else if (!$reservation->player_assignment) {

                            //Get Count
                            $my_reservation = reservation::where('matchup_id', '=', $matchup[0]->id)
                                ->whereNotNull('player_assignment')
                                ->get();
                            
                            $current_count = sizeof($my_reservation);
                            
                            //Set this player with the next assignment
                            $reservation->player_assignment = $current_count + 1;
                        }
                        
                        //Else show their instructions based on assignment number
                    }
                }

                //Change status to unavailable
                else if ($reservation->reservation_status == 1) {
                    $reservation->reservation_status = 0;
                }
                
                //Change status back to available
                else if ($reservation->reservation_status == 0) {
                    $reservation->reservation_status = 1;
                }
            }
            else {
                $reservation = new reservation;
                $reservation->matchup_id = $matchup[0]->id;
                $reservation->member_id = $member[0]->id;
                $reservation->checkin_status = 0;
                $reservation->reservation_status = 1;
            }
            
            $reservation->save();

            if ($matchup[0]->date <= Carbon::now()->addHour($this->off_set_checkin)->toDateTimeString()) {
                return $this->returnBattleView($alliance_id, $matchup[0], $reservation, $member_name);
            }
            else {
                return view('success', ['membername' => $member_name]);
            }
            
        }       
    }

    public function returnBattleView($alliance_id, $matchup, $reservation, $member_name) {

        $max_players_per_group = 4;

        //Check to see if this player is a rally leader
        if ($reservation->rally_assignment) {
            $type = "Rally_" . $reservation->rally_assignment;
            $leader = $member_name;
            $group = $max_players_per_group * $reservation->rally_assignment + 1;
        }
        else if ($reservation->player_assignment) {
            if ($reservation->player_assignment < 5) {
                $type = "Group_1";
                $lead = 1;
                $group = $max_players_per_group * $lead + 1;
            }
            else if ($reservation->player_assignment < 9) {
                $type = "Group_2";
                $lead = 2;
                $group = $max_players_per_group * $lead + 1;

            }
            else if ($reservation->player_assignment < 13) {
                $type = "Group_3";
                $lead = 3;
                $group = $max_players_per_group * $lead + 1;

            }
            else if ($reservation->player_assignment < 17) {
                $type = "Group_4";
                $lead = 4;
                $group = $max_players_per_group * $lead + 1;

            }
            else if ($reservation->player_assignment < 21) {
                $type = "Group_5";
                $lead = 5;
                $group = $max_players_per_group * $lead + 1;

            }
            else if ($reservation->player_assignment < 25) {
                $type = "Group_6";
                $lead = 6;
                $group = $max_players_per_group * $lead + 1;

            }
        }


        //Set Team Leaders and Teammates
        //If Player is a lead
        if (isset($leader)) {

            $teammates = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.matchup_id', $matchup->id)
                    ->where('reservations.player_assignment', '<', $group)
                    ->where('reservations.player_assignment', '>=', $group - $max_players_per_group)
                    ->select('members.name')
                    ->get();
        }
        //If player is not a lead
        else {

            //Find the leader
            $leader = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.matchup_id', $matchup->id)
                    ->where('reservations.rally_assignment', $lead)
                    ->select('members.name')
                    ->first();

            $leader = $leader->name;
            
            $teammates = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.matchup_id', $matchup->id)
                    ->where('reservations.player_assignment', '<', $group)
                    ->where('reservations.player_assignment', '>=', $group - $max_players_per_group)
                    ->select('members.name')
                    ->get();
        }

        //Check for Color
        if ($matchup->team == 'Blue') {
            $team = 1;
        }
        else {
            $team = 2;
        }

        //Check for Language
        
        $member_language = member_language::where('user_id', $reservation->member_id)->first();

        if ($member_language != null) {
            $language = $member_language->language;

            //Spanish
            if ($language == 'es') {
                $strategy = instructions_es::where([
                    ['type', '=', $type],
                    ['aid', '=', $alliance_id],
                    ['color', '=', $team],
                    ['opponent_strength', '=', $matchup->strategy]
                ])->first();
            }
        }

        //English
        else {
            $strategy = instructions::where([
                ['type', '=', $type],
                ['aid', '=', $alliance_id],
                ['color', '=', $team],
                ['opponent_strength', '=', $matchup->strategy]
            ])->first();
        }

        error_log("Leader: " . $leader);
        error_log('Members: ' . $teammates);

        return view('battleplan', ['membername' => $member_name, 'strategy' => $strategy, 'leader' => $leader, 'members' => $teammates]);
    }

    //Show team Output 
    public function outputTeams ($alliance_id) {

        $matchup = matchup::where([
            ['date', '>', Carbon::now()->toDateTimeString()],
            ['aid', '=', $alliance_id]
        ])->first();        

        if ($matchup != null) {

            //Number of Teams
            $teams = 6;
            $num_members_per_team = 4;
            $final_team = [];

            for ($x = 1; $x <= $teams; $x++) {
                
                $leader = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.matchup_id', $matchup->id)
                    ->where('reservations.rally_assignment', $x)
                    ->select('members.name')
                    ->first();

                $leader = $leader->name;
                
                $teammates = DB::table('reservations')
                        ->join('members', 'reservations.member_id', '=', 'members.id')
                        ->where('reservations.matchup_id', $matchup->id)
                        ->where('reservations.player_assignment', '<', $x * $num_members_per_team + 1)
                        ->where('reservations.player_assignment', '>=', $x * $num_members_per_team - $num_members_per_team + 1)
                        ->select('members.name')
                        ->get();
                
                $team = [];
                $team['leader'] = $leader;
                $team['members'] = $teammates;

                array_push($final_team, $team);
            }

            //dd($final_team);

            return view('printout', ['teams' => $final_team]);

        }
    }

    /**
     * Return TimeZone of DAte provided
     *
     */
    public function viewLineup($alliance_id) 
    {
        
        $matchup = matchup::where([
            ['date', '>', Carbon::now()->toDateTimeString()],
            ['aid', '=', $alliance_id]
        ])->get();

        if (sizeof($matchup) < 1) {
            return view('assign');
        }

        else {
            $members = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.matchup_id', $matchup[0]->id)
                    ->where('reservations.reservation_status', 1)
                    ->select('reservations.id', 'reservations.checkin_status', 'reservations.rally_assignment', 'members.id', 'members.name')
                    ->orderBy('members.name', 'asc')
                    ->get();

            if (sizeof($members) > 0) {
                return view('assign', ['matchup_id' => $matchup[0]->id , 'members' => $members]);
            }
            else {
                return view('assign');
            }
        }
    }

    /**
     * Form Control for Setting Lineup
     *
     */
    public function setLineup (Request $request) {

        $matchup_id  = $request->input('matchup_id');
        $raiders = $request->input('draw_into_raid');
        $leads = $request->get('rally_leads');

        $leads = array_diff($leads, array("None"));

        error_log("Matchup ID: " . $matchup_id);
        //error_log("Size of Raiders: " . sizeof($raiders));
        //error_log("Size of leads: " . sizeof($leads));
        
        //Section to mark those who are available for checkin to get an assigned number
        $members_that_reserved = reservation::where('matchup_id', $matchup_id)->get();

        foreach ($members_that_reserved as $member) {

            $reservation = reservation::where([
                ['matchup_id', '=', $matchup_id],
                ['member_id', '=', $member->member_id]            
            ])->first();
        
            if (!empty($raiders) && in_array($member->member_id, $raiders)) {
                $reservation->checkin_status = 1;
            }
            else {
                $reservation->checkin_status = 0;
            }
            
            //Reset all Rally Leaders
            $reservation->rally_assignment = NULL;

            $reservation->save();    
        }

        //Set Rally Assignments
        foreach ($leads as $rally_leader) {

            list($rally_number, $member_id) = explode('_', $rally_leader);

            $reservation = reservation::where([
                ['matchup_id', '=', $matchup_id],
                ['member_id', '=', $member_id]            
            ])->first();

            $reservation->rally_assignment = $rally_number;

            $reservation->save();
        }

        $matchup = matchup::find($matchup_id);

        return Redirect::to('/viewLineup/' . $matchup->aid);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(reservation $reservation)
    {
        //
    }

    //Statistics
    public function statistics ($alliance_id) {

        $reserved_members = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.reservation_status', 1)
                    ->where('members.aid', $alliance_id)
                    ->select(DB::raw('count(members.name) as reservations'), 'members.name')
                    ->groupBy('members.name')
                    ->get();

        $reserved_members = $this->convertArray($reserved_members);
        
        $checkedin_members = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.checkin_status', 1)
                    ->where('members.aid', $alliance_id)
                    ->select(DB::raw('count(members.name) as lineup'), 'members.name')
                    ->groupBy('members.name')
                    ->get();
        
        $checkedin_members = $this->convertArray($checkedin_members);

        $absent_members = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->where('reservations.absent', 1)
                    ->where('members.aid', $alliance_id)
                    ->select(DB::raw('count(members.name) as absent'), 'members.name')
                    ->groupBy('members.name')
                    ->get();

        $absent_members = $this->convertArray($absent_members);


        $latest_matches = DB::table('reservations')
                    ->join('members', 'reservations.member_id', '=', 'members.id')
                    ->join('matchups', 'reservations.matchup_id', '=', 'matchups.id')
                    ->where('reservations.checkin_status', 1)
                    ->where('members.aid', $alliance_id)
                    ->select('matchups.date', 'members.name')
                    ->orderBy('matchups.date', 'desc')
                    ->get();

        $latest_matches = $this->convertArray($latest_matches);

        //Get Members
        $all_members = member::all()->sortBy("name");

        $members = [];

        foreach ($all_members as $cur_member) {

            $name = $cur_member->name;

            $temp_member['name'] = $name;

            $temp_member['reservations'] = isset($reserved_members[$name]) ? $reserved_members[$name] : '-';
            $temp_member['lineup'] = isset($checkedin_members[$name]) ? $checkedin_members[$name] : '-';
            $temp_member['absent'] = isset($absent_members[$name]) ? $absent_members[$name] : '-';
            $temp_member['last'] = isset($latest_matches[$name]) ? $latest_matches[$name] : '-';

            array_push($members, $temp_member);
        }

        return view('statistics', ['members' => $members]);
    }

    //Convert Array to Associative Array
    private function convertArray ($array) {

        foreach ($array as $item) {
            $name = '';
            $value = '';

            foreach ($item as $key => $val) {

                if ($key == 'name') {
                    $name = $val;
                }
                else {
                    $value = $val;
                }
            }
            $new_array[$name] = $value;
        }
        
        return $new_array;
    }
}
