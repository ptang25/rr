<?php

namespace App\Http\Controllers;

use App\member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index($aid)
    {
        $members = member::where('aid', $aid)->orderBy('name', 'asc')->get();

        return view('members', ['alliance_id' => $aid, 'members' => $members]);
    }

    //Create a Category
    public function createMember (Request $request) {
    
        if ($request->input('member_id') != null) {
            $member = member::find($request->input('member_id'));
            error_log("ID is : " . $member->id);
        }
        //Get Next ID
        else {
            $member = new member();
        }

        //Get alliance ID
        $allianceID = $request->input('alliance_id');

        //error_log("My Config ID is : " . $option->id);
        $name = $request->input('name');
    
        error_log("Alliance is : " . $allianceID);
        error_log("Name is : " . $name);
    
        $member->aid = $allianceID;
        $member->name = $name;

        $member->save();

        //Return View
        $members = member::where('aid', $allianceID)->orderBy('name', 'asc')->get();

        return view('members', ['alliance_id' => $allianceID , 'members' => $members]);
    }

    //Delete a Category
    public function deleteMember ($mid) {
    
        $member = member::find($mid);
        $aid  = $member->aid;

        $member->delete();

        //Return View
        $members = member::where('aid', $aid)->orderBy('name', 'asc')->get();

        return view('members', ['alliance_id' => $aid, 'members' => $members]);
    }
}
