<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAlliance extends Model
{
    protected $table = "user_alliance";

    protected $fillable = [
        'aid', 'uid'
    ];
}
