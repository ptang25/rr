<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instructions_es extends Model
{
    protected $table = "instructions_es";

    protected $fillable = [
        'header', 'pre_image', 'pre_text', 'post_image', 'post_text', 'footer'
    ];
}
