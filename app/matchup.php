<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class matchup extends Model
{
    protected $table = "matchups";

    protected $fillable = [
        'aid', 'date', 'team'
    ];
}
