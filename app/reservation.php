<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reservation extends Model
{
    protected $table = "reservations";

    protected $fillable = [
       'matchup_id', 'member_id', 'reservation_status', 'checkin_status', 'player_assignment', 'rally_assignment'
    ];
}
