@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <h4 class="centertext">Set Lineup</h4>
        <form method="POST" enctype="multipart/form-data" id="members_form" action="/setLineup">
            @csrf
            @isset($matchup_id)
                <input type="hidden" id="matchup_id" name="matchup_id" value="{{ $matchup_id }}">
            @endisset

            <div class="row categorySpc">
                <div class="col-3">
                    <p class="categoryName RallyLeadsSpc">Name</p>
                </div>
                <div class="col-3">
                    <p class="categoryName RallyLeadsSpc">Raiding?</p>
                </div>
                <div class="col-6">
                    <p class="categoryName RallyLeadsSpc">Rally Lead?</p>
                </div>
            </div>

            @isset($members)
                @foreach ($members as $mymember)
                    <div class="row categorySpc">
                        <div class="col-4">
                            <p class="categoryName RallyLeadsSpc">{{ $mymember->name}}</p>
                        </div>
                        <div class="col-2">
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox" name="draw_into_raid[]" value={{ $mymember->id }} 
                                    @if ($mymember->checkin_status == 1)
                                        checked="checked"
                                    @endif
                                >
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="col-6">
                            <!-- Rally Leader -->
                            <select id="rally_leads" name="rally_leads[]" class="selectRallyLeads border-bottom-input">
                                <option value="None">None</option>
                                <option value="1_{{ $mymember->id }}"
                                    @if ($mymember->rally_assignment == 1)
                                            selected
                                    @endif
                                >Rally Leader 1</option>
                                <option value="2_{{ $mymember->id }}"
                                    @if ($mymember->rally_assignment == 2)
                                            selected
                                    @endif
                                >Rally Leader 2</option>
                                <option value="3_{{ $mymember->id }}"
                                    @if ($mymember->rally_assignment == 3)
                                            selected
                                    @endif
                                >Rally Leader 3</option>
                                <option value="4_{{ $mymember->id }}"
                                    @if ($mymember->rally_assignment == 4)
                                            selected
                                    @endif
                                >Rally Leader 4</option>
                                <option value="5_{{ $mymember->id }}"
                                    @if ($mymember->rally_assignment == 5)
                                            selected
                                    @endif
                                >Rally Leader 5</option>
                                <option value="6_{{ $mymember->id }}"
                                    @if ($mymember->rally_assignment == 6)
                                            selected
                                    @endif
                                >Rally Leader 6</option>
                            </select>
                        </div>
                    </div>
                @endforeach
            @endisset
            
            <div class="row justify-content-center">
                <input type="submit" value="Set Raid Lineup">
            </div>
        </form>
        
        <div class="row justify-content-center m-3">
            <a href="/home"><input type='button' value='Go Back'/></a>
        </div>

        <hr/>
        
        

        

    </div>
@endsection