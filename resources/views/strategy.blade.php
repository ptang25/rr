@extends('layouts.app')

@section('content')
<div class="wrapper">
    <h4 class="centertext">Set Strategy</h4>

    @isset($strategies)
        <div class="row categorySpc">
            <div class="col">
                <p class="categoryName RallyLeadsSpc">Strategy For</p>
            </div>
            <div class="col">
                <p class="categoryName RallyLeadsSpc">Team Color</p>
            </div>
            <div class="col">
                <p class="categoryName RallyLeadsSpc">Edit Strategy</p>
            </div>
        </div>

        @foreach ($strategies as $strategy)
            <div class="row justify-content-center categorySpc">
                <div class="col">
                    <p class="categoryName">{{ $strategy->type}}</p>
                </div>
                <div class="col">
                    <p class="categoryName">
                            @if ($strategy->color == 1)
                                Blue
                            @else
                                Red
                            @endif
                    </p>
                </div>
                <div class="col">
                    <a href="/editStrategy/{{ $alliance_id }}/{{ $strategy->id }}" class="btn btn-primary">Edit</a>
                </div>
            </div>
        @endforeach
    @endisset

    <hr/>

    <form method="POST" enctype="multipart/form-data" id="createStrategy" action="/createStrategy">
        @csrf
        @isset($instructions)
            <input type="hidden" id="instructions_id" name="instructions_id" value="{{ $instructions->id }}">
        @endisset
        <input type="hidden" id="alliance_id" name="alliance_id" value="{{ $alliance_id }}">
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="timezone">Group:</label>
            </div>
            <div class="col-8">
                <select id="group" name="group" class="border-bottom-input">
                    <option value="Rally_1"
                        @isset($instructions)
                            @if ($instructions->type == "Rally_1")
                                selected
                            @endif                           
                        @endisset
                    >Rally Leader 1</option>
                    <option value="Rally_2"
                        @isset($instructions)
                            @if ($instructions->type == "Rally_2")
                                selected
                            @endif                           
                        @endisset
                    >Rally Leader 2</option>
                    <option value="Rally_3"
                        @isset($instructions)
                            @if ($instructions->type == "Rally_3")
                                selected
                            @endif                           
                        @endisset
                    >Rally Leader 3</option>
                    <option value="Rally_4"
                        @isset($instructions)
                            @if ($instructions->type == "Rally_4")
                                selected
                            @endif                           
                        @endisset
                    >Rally Leader 4</option>
                    <option value="Rally_5"
                        @isset($instructions)
                            @if ($instructions->type == "Rally_5")
                                selected
                            @endif                           
                        @endisset
                    >Rally Leader 5</option>
                    <option value="Rally_6"
                        @isset($instructions)
                            @if ($instructions->type == "Rally_6")
                                selected
                            @endif                           
                        @endisset
                    >Rally Leader 6</option>
                    <option value="Group_1"
                        @isset($instructions)
                            @if ($instructions->type == "Group_1")
                                selected
                            @endif                           
                        @endisset
                    >Group 1</option>
                    <option value="Group_2"
                        @isset($instructions)
                            @if ($instructions->type == "Group_2")
                                selected
                            @endif                           
                        @endisset
                    >Group 2</option>
                    <option value="Group_3"
                        @isset($instructions)
                            @if ($instructions->type == "Group_3")
                                selected
                            @endif                           
                        @endisset
                    >Group 3</option>
                    <option value="Group_4"
                        @isset($instructions)
                            @if ($instructions->type == "Group_4")
                                selected
                            @endif                           
                        @endisset
                    >Group 4</option>
                    <option value="Group_5"
                        @isset($instructions)
                            @if ($instructions->type == "Group_5")
                                selected
                            @endif                           
                        @endisset
                    >Group 5</option>
                    <option value="Group_6"
                        @isset($instructions)
                            @if ($instructions->type == "Group_6")
                                selected
                            @endif                           
                        @endisset
                    >Group 6</option>
                </select>    
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="team">Team Color:</label>
            </div>
            <div class='col-8'>
                <select id="team" name="team" class="border-bottom-input">
                    <option value="1"
                        @isset($instructions)
                            @if ($instructions->color == 1)
                                selected
                            @endif                           
                        @endisset
                    >Blue</option>
                    <option value="2"
                        @isset($instructions)
                            @if ($instructions->color == 2)
                                selected
                            @endif                           
                        @endisset
                    >Red</option>
                </select>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="province">Top Instructions:</label>
            </div>
            <div class="col-8">
                <textarea cols="50" rows="10" placeholder="Instructions"
                    id="top_instructions" name="top_instructions">@isset($instructions){{ $instructions->header }}@endisset
                </textarea>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="image">Stage 1 Image:</label>
            </div>
            <div class="col-8">
                <img id="pre_image_preview" src=
                    @isset($instructions)
                        '{{ URL::asset($instructions->pre_image)}}'
                    @else
                        "/img/file-upload-solid.svg"
                    @endisset    
                        alt="preview image" style="max-height: 125px;"> <br/>
                <div class="centerFileDiv">    
                    <input type="file" name="pre_image" id="pre_image" class="border-bottom-input">
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="province">Stage 1 Instructions:</label>
            </div>
            <div class="col-8">
                <textarea cols="50" rows="10" placeholder="Instructions"
                    id="stage1_instructions" name="stage1_instructions">@isset($instructions){{ $instructions->pre_text }}@endisset
                </textarea>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="image">Final Stage Image:</label>
            </div>
            <div class="col-8">
                <img id="post_image_preview" src=
                    @isset($instructions)
                        '{{ URL::asset($instructions->post_image)}}'
                    @else
                        "/img/file-upload-solid.svg"
                    @endisset    
                        alt="preview image" style="max-height: 125px;"> <br/>
                <div class="centerFileDiv">    
                    <input type="file" name="post_image" id="post_image" class="border-bottom-input">
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="province">Final Stage Instructions:</label>
            </div>
            <div class="col-8">
                <textarea cols="50" rows="10" placeholder="Instructions"
                    id="stage2_instructions" name="stage2_instructions">@isset($instructions){{ $instructions->post_text }}@endisset
                </textarea>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-4">
                <label for="province">Tips:</label>
            </div>
            <div class="col-8">
                <textarea cols="50" rows="10" placeholder="Tips"
                    id="footer" name="footer">@isset($instructions){{ $instructions->footer }}@endisset
                </textarea>
            </div>
        </div>
        <div class="row justify-content-center">
            <input type="submit" value=
                @isset($instructions)
                    "Update Strategy"
                @else
                    "Create Strategy"
                @endisset   
            >
        </div>
    </form>
    
    <div class="row justify-content-center m-3">
        <a href="/home"><input type='button' value='Go Back'/></a>
    </div>

</div>
@endsection

@section('myscripts')
    <script type="text/javascript">
        
        $(document).ready(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#pre_image').change(function(){
            
                let reader = new FileReader();
                reader.onload = (e) => { 
                $('#pre_image_preview').attr('src', e.target.result); 
                }
                reader.readAsDataURL(this.files[0]); 

            });

            $('#post_image').change(function(){
            
            let reader = new FileReader();
            reader.onload = (e) => { 
            $('#post_image_preview').attr('src', e.target.result); 
            }
            reader.readAsDataURL(this.files[0]); 

        });

    });

    </script>
@endsection