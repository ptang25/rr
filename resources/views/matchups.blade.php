@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <h4 class="centertext">Matchup Details</h4>
        <form method="POST" enctype="multipart/form-data" id="matchup_form" action="/matchupOptions">
            @csrf
            @isset($allianceID)
                <input type="hidden" id="alliance_id" name="alliance_id" value={{ $allianceID }}>
            @endisset
            @isset($matchupID)
                <input type="hidden" id="matchup_id" name="matchup_id" value={{ $matchupID }}>
            @endisset
            <div class="row justify-content-center">
                <label for="date">Matchup Date (UTC):</label>
            </div>
            <div class="row justify-content-center">
                <div class='col-sm-5'>
                    <input type="date" class="form-control" id="date" name="date"
                    @isset($date)
                        value={{ $date }} 
                    @endisset>
                </div>
            </div>
            <div class="row justify-content-center">
                <label for="date">Matchup Time (UTC):</label>
            </div>
            <div class='row justify-content-center'>
                <div class='col-sm-1'>
                    <input type="text" id="hour" name="hour" placeholder="HH" 
                    @isset($hour)
                        value={{ $hour }} 
                    @endisset
                        class="border-bottom-input">
                </div>
                <p class='timeseperator'>:</p>
                <div class='col-sm-1'>
                    <input type="text" id="minute" name="minute" placeholder="MM" 
                    @isset($minute)
                        value={{ $minute }} 
                    @endisset
                        class="border-bottom-input">
                </div>
            </div>
            <div class="row justify-content-center">
                <label for="teamcolor">Team Color:</label>
            </div>
            <div class="row justify-content-center">
                <div class="col-sm-2">
                    <select id="teamcolor" name="teamcolor" class="border-bottom-input">
                        <option value="Blue"
                            @isset($team)
                                @if ($team == "Blue")
                                    selected
                                @endif
                            @endisset
                        >Blue</option>
                        <option value="Red"
                            @isset($team)
                                @if ($team == "Red")
                                    selected
                                @endif
                            @endisset
                        >Red</option>
                    </select>
                </div>
            </div>

            <div class="row justify-content-center">
                <input type="submit" value=
                    @isset($config)
                        "Update Matchup"
                    @else
                        "Confirm Matchup"
                    @endisset 
                >
            </div>
        </form>

        <div class="row justify-content-center m-3">
            <a href="/home"><input type='button' value='Go Back'/></a>
        </div>
    </div>
@endsection
