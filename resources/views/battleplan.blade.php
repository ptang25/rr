@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row justify-content-center">
        <h3>
            Welcome {{ $membername }}
        </h3>
    </div>
    <div class="row justify-content-center">
        <h4>
            @if ($strategy->type == "Rally_1")
                You are Rally Leader #1
            @elseif ($strategy->type == "Rally_2")
                You are Rally Leader #2
            @elseif ($strategy->type == "Rally_3")
                You are Rally Leader #3
            @elseif ($strategy->type == "Rally_4")
                You are Rally Leader #4
            @elseif ($strategy->type == "Rally_5")
                You are Rally Leader #5
            @elseif ($strategy->type == "Rally_6")
                You are Rally Leader #6
            @endif
        </h4>
    </div>

    <div class="row justify-content-center mt-3">
        <h5>
            Your Team Lead
        </h5>
    </div>

    <div class="row justify-content-center mt-3">
        <p class="formatted-text centertext">{{ $leader }}</p>
    </div>

    <div class="row justify-content-center mt-3 mb-3">
        <h5>
            Your Team Members
        </h5>
    </div>

    @foreach ($members as $mymember)
        <div class="row justify-content-center">
            <p class="centertext">{{ $mymember->name }}</p>
        </div>
    @endforeach

    <div class="row justify-content-center mt-3">
        <h5>
            Outline
        </h5>
    </div>

    <div class="row justify-content-center mt-3">
        <p class="formatted-text centertext">{{ htmlentities($strategy->header) }}</p>
    </div>

    <div class="row justify-content-center">
        <img id="pre_image_preview" src='{{ URL::asset($strategy->pre_image)}}' alt="preview image" style="max-height: 200px;">
    </div>

    <div class="row justify-content-center mt-3">
        <h5>
            When the Raid Opens
        </h5>
    </div>

    <div class="row justify-content-center mt-3">
        <p class="formatted-text centertext">{{ htmlentities($strategy->pre_text) }}</p>
    </div>

    <div class="row justify-content-center mt-3">
        <img id="post_image_preview" src='{{ URL::asset($strategy->post_image)}}' alt="preview image" style="max-height: 200px;">
    </div>

    <div class="row justify-content-center mt-3">
        <h5>
            When the Central Reservoir Opens
        </h5>
    </div>

    <div class="row justify-content-center mt-3">
        <p class="formatted-text centertext">{{ htmlentities($strategy->post_text) }}</p>
    </div>

    <div class="row justify-content-center mt-3">
        <h5>
            Tips
        </h5>
    </div>

    <div class="row justify-content-center mt-3">
        <p class="formatted-text centertext">{{ htmlentities($strategy->footer) }}</p>
    </div>
    

</div>
@endsection
