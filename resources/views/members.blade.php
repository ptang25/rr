@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <h4 class="centertext">Members</h4>
        <form method="POST" enctype="multipart/form-data" id="members_form" action="/createMember">
            @csrf
            @isset($member)
                <input type="hidden" id="member_id" name="member_id" value="{{ $member->id }}">
            @endisset
            <input type="hidden" id="alliance_id" name="alliance_id" value="{{ $alliance_id }}">
            <div class="row justify-content-center">
                <label for="name">Name</label>
            </div>
            <div class="row justify-content-center">   
                <input type="text" id="name" name="name" class="optionsInput"
                    @isset($member)
                        value='{{$member->name}}' 
                    @endisset
                        class="border-bottom-input">
            </div>
            <div class="row justify-content-center">
                <input type="submit" value=
                    @isset($mid)
                        "Update Member"
                    @else
                        "Create Member "
                    @endisset   
                >
            </div>
        </form>
        
        <div class="row justify-content-center m-3">
            <a href="/home"><input type='button' value='Go Back'/></a>
        </div>

        <hr/>
        
        @isset($members)
            @foreach ($members as $mymember)
                <div class="row categorySpc">
                    <div class="col-4 membersSpc">
                        <p class="categoryName">{{ $mymember->name}}</p>
                    </div>
                    <div class="col-2">
                        <a href="/editMember/{{ $mymember->id }}" class="btn btn-primary">Edit</a>
                    </div>
                    <div class="col-2">
                        <a href="/removeMember/{{ $mymember->id }}" class="btn btn-danger">Remove</a>
                    </div>
                </div>
            @endforeach
        @endisset

        

    </div>
@endsection