@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <h5 class="centertext">
            Thank you {{ $membername }}, you have successfully indicated your availability to attend this Reservoir Raid.
        </h5>
    </div>


</div>
@endsection
