@extends('layouts.app')

@section('content')
<div class="container">

    @foreach ($teams as $team)  
        <div class="row justify-content-center mt-5">
            <h5 class="centertext">
                {{ $team['leader'] }}    
                <br />       
                @foreach ($team['members'] as $member) 
                {{ $member->name }}     
                <br />             
                @endforeach 
            </h5>
        </div>
    @endforeach


</div>
@endsection
