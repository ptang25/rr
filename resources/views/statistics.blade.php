@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <h4 class="centertext">Statistics</h4>

            <div class="row categorySpc">
                <div class="col-3">
                    <p class="categoryName RallyLeadsSpc">Name</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">Reservations Made</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">In Lineup</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">Absent</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">Last Match</p>
                </div>
            </div>

            @foreach ($members as $member)
            <div class="row categorySpc">
                <div class="col-3">
                    <p class="categoryName RallyLeadsSpc">{{ $member['name']}}</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">{{ $member['reservations']}}</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">{{ $member['lineup']}}</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">{{ $member['absent']}}</p>
                </div>
                <div class="col-2">
                    <p class="categoryName RallyLeadsSpc">{{ $member['last']}}</p>
                </div>
            </div>            
            @endforeach
            
        </form>
        
        <div class="row justify-content-center m-3">
            <a href="/home"><input type='button' value='Go Back'/></a>
        </div>

        <hr/>   

    </div>
@endsection