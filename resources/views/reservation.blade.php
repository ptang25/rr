@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <h4 class="centertext rrtitle">RR Reservation System</h4>
        <form method="POST" enctype="multipart/form-data" id="members_form" action="/setReservation">
            @csrf
            <input type="hidden" id="alliance_id" name="alliance_id" value="{{ $alliance_id }}">
            <div class="row justify-content-center mt-5">
                <h5>Select Your Name</h5>
            </div>

            <div class="row justify-content-center">
                <div class="col-sm-2">
                    <select id="member_name" name="member_name" class="form-control border-bottom-input">
                        @isset($members)
                            @foreach ($members as $mymember)
                                <option value='{{ $mymember->name }}'>{{ $mymember->name }}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>
            </div>

            <hr />

            <!-- Display Matchup Date !-->
            @isset($matchup)
                <div class="row justify-content-center">
                        <label>Matchup Date:</label>
                </div>
                <div class="row justify-content-center">
                        <label class="matchup_date_title" id="matchupDate">{{ DateTime::createFromFormat("Y-m-d H:i:s", $matchup->date)->format("Y-M-d H:i:s") }}</label>

                </div>
                <div class="row justify-content-center timezone_select">
                    <div class="col-sm-5">
                        <label class='row justify-content-center'>Change Timezone to:</label>
                        <br />
                        <select id="matchup_timezone" name="matchup_timezone" class="form-control border-bottom-input">
                            <option value='UTC'>Universal Time Coordinated (UTC)</option>
                            <option value='1'>Pacific Standard Time</option>
                            <option value='2'>Mountain Standard Time</option>
                            <option value='3'>Central Standard Time</option>
                            <option value='4'>Eastern Standard Time</option>
                            <option value='5'>Australia/Melbourne</option>
                            <option value='6'>England/London</option>
                            <option value='7'>France/Paris</option>
                            <option value='8'>Indonesia/Jakarta</option>
                            <option value='9'>Philippines/Manila</option>
                            <option value='10'>Sweden/Stockholm</option>
                        </select>
                    </div>
                </div>

                @isset($reservations)
                    <div class="row justify-content-center">
                        <label>Currently {{ $reservations }} reservations made for this event.</label>
                    </div>
                @endisset

                <div class="row justify-content-center">
                    <label id="refText" name="refText"></label>
                </div>
   
            @else
                <div class="row justify-content-center">
                    <label id="refText" name="refText">No Matchup Date Available yet</label>
                </div>
            @endisset

            <div class="row justify-content-center">
                <input type="submit" id="submit" value="Yes">
            </div>
        </form>
        
        <div class="row justify-content-center m-3">
            <a href="/home"><input type='button' value='Go Back'/></a>
        </div>        

    </div>
@endsection

@section('scripts')

<script type="text/javascript">

    $(document).ready(function() {
        $('select[name="matchup_timezone"]').on('change', function() {

            var timezone = $(this).val();

            if(timezone) {

                $.ajax({

                    url: '/getTimezone/'+timezone+'/'+{{ $matchup->id ?? '' }},

                    type: "GET",

                    dataType: "json",

                    success:function(data) {

                        $('#matchupDate').addClass("reservationcolor");                        
                        $('#matchupDate').text(data);

                    }

                });
            }

        });

        $('select[name="member_name"]').on('change', function() {

            var name = $(this).val();

            if(name) {

                $.ajax({

                    url: '/getMemberStatus/'+name+'/'+{{ $alliance_id }},

                    type: "GET",

                    dataType: "json",

                    success:function(data) {

                        //Disable Button
                        if (data['statement'] == "Cutoff Reservation Time has expired, Sorry!") {
                            $("#submit").attr("disabled", true);
                        }
                        else if (data['statement'] == "Our apologies, you were not selected to the Raid this time, we will fit in the next one, thank you for reserving.") {
                            $("#submit").attr("disabled", true);
                        }
                        else {
                            $("#submit").attr("disabled", false);
                        }
                        //$('#refText').addClass("reservationcolor");  
                        $('#refText').css("font-size", "1.5rem");  
                        $('#refText').css("color", data['color']);
                        $('#refText').text(data['statement']);
                    }

                });
            }

        });

    });

    </script>
@endsection