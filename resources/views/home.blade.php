@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Welcome {{ Auth::user()->name }}
                </div>
            </div>
        </div>
    </div>

    @isset($alliance)

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <h5 class="card-title center">
                      Set Strategy
                  </h5>
                  <p class="card-text centertext">Current Strategies</p>
                  <div class="centerbutton">
                    <a href="/strategies/{{ $alliance->aid }}" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <h5 class="card-title center">
                      Matchup Details
                  </h5>
                  <p class="card-text centertext">Set the Time and Date of the Matchup</p>
                  <div class="centerbutton">
                    <a href="/matchup" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <h5 class="card-title center">
                        Edit Roster
                  </h5>
                  <p class="card-text centertext">Confirm the participant</p>
                  <div class="centerbutton">
                    <a href="/members/{{ $alliance->aid }}" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <h5 class="card-title center">
                      View Confirmations
                  </h5>
                  <p class="card-text centertext">Active List of Confirmed Participants</p>
                  <div class="centerbutton">
                  <a href="/viewLineup/{{ $alliance->aid }}" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <h5 class="card-title center">
                      Statistics                      
                  </h5>
                  <p class="card-text centertext">Tally of registrants and confirmations</p>
                  <div class="centerbutton">
                  <a href="/statistics/{{ $alliance->aid }}" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 21rem;">
                <div class="card-body">
                  <h5 class="card-title center">
                      Show Teams
                  </h5>
                  <p class="card-text centertext">Teams for upcoming Match</p>
                  <div class="centerbutton">
                  <a href="/showTeams/{{ $alliance->aid }}" class="btn btn-primary">Click Here</a>
                  </div>
                </div>
            </div>
        </div>
    </div>

    @else
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <p class="centertext">No Alliance Set Yet, please wait while we validate your profile</p>
                    </div>
                </div>
            </div>
        </div>
        
    @endisset

</div>
@endsection
