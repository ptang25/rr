<?php

use App\member;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/strategies/{aid}', 'StrategyController@viewStrategy')->middleware('auth');
Route::post('/createStrategy', 'StrategyController@createStrategy')->middleware('auth');
Route::get('/editStrategy/{aid}/{id}', 'StrategyController@editStrategy')->middleware('auth');
Route::get('/removeStrategy/{aid}/{id}', 'StrategyController@removeStrategy')->middleware('auth');




Route::get('/matchup', 'MatchupController@index')->middleware('auth');
Route::post('/matchupOptions', 'MatchupController@storeMatchup')->middleware('auth');

Route::get('/members/{aid}', 'MemberController@index')->middleware('auth');
Route::post('/createMember', 'MemberController@createMember')->middleware('auth');
Route::get('/removeMember/{id}', 'MemberController@deleteMember')->middleware('auth');
Route::get('/editMember/{id}', function ($id) {

    $member = member::find($id);
    $aid  = $member->aid;

    //Return View
    $members = member::where('aid', $aid)->get();

    return view('members', ['alliance_id' => $aid, 'members' => $members, 'member' => $member]);

   
})->middleware('auth');

Route::get('/reservation/{id}', 'ReservationController@index');
Route::get('/getMemberStatus/{name}/{aid}', 'ReservationController@memberStatus');
Route::get('/getTimezone/{timezone}/{date}', 'ReservationController@getTimeZone');

//Set Reservation
Route::post('/setReservation', 'ReservationController@setReservation');

//Admin Set Lineup
Route::get('/viewLineup/{aid}', 'ReservationController@viewLineup')->middleware('auth');
Route::post('/setLineup', 'ReservationController@setLineup')->middleware('auth');

//Show Printout
Route::get('/showTeams/{aid}', 'ReservationController@outputTeams')->middleware('auth');

//Show Stats
Route::get('/statistics/{aid}', 'ReservationController@statistics')->middleware('auth');